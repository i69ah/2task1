<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/style/index.css">
    <title>News</title>
</head>
<body>
<div class="wrapper">
    <?php
    foreach ($news as $article) {
        ?>
        <a href="/article.php?id=<?= $article->id ?>">
            <div class="article">
                <div class="article__title">
                    <?= $article->title ?>
                </div>
                <div class="article__content">
                    <?= $article->content ?>
                </div>
            </div>
        </a>
        <?php
    }
    ?>
</div>
</body>
</html>