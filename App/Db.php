<?php
namespace App;

class Db
{
    private \PDO $dbh;

    /**
     * Db constructor.
     */
    public function __construct()
    {
        $config = (include __DIR__ .'/../config.php')['db'];

        if (
            empty($config['host']) && empty($config['dbname'])
            && empty($config['user']) && empty($config['password'])
        ) {
            throw new \Exception(
                'Could not create an object of the type DB' . PHP_EOL .
                ' Check whether the default configuration parameters exist'
            );
        }

        $this->dbh = new \PDO(
            'mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'],
            $config['user'], $config['password']
        );
    }


    /**
     * Get the sql query result with the array of $class objects
     * @param string $sql
     * @param string $class
     * @param array $params
     * @return array|null
     */
    public function query(string $sql, string $class, array $params=[]): ?array
    {
        $sth = $this->dbh->prepare($sql);
        if ($sth->execute($params)) {
           return $sth->fetchAll(\PDO::FETCH_CLASS, $class);
        }
        return [];
    }


    /**
     * Executes sql command
     * @param string $sql
     * @param array $params
     * @return bool
     * Returns true in case of success or false in case of failure
     */
    public function execute(string $sql, array $params=[]): bool
    {
        return $this->dbh->prepare($sql)->execute($params);
    }
}